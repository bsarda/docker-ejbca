# written by Benoit Sarda
# ejbca container. uses bsarda/jboss by copy/paste.
#
#   bsarda <b.sarda@free.fr>
#
FROM centos:centos7.2.1511
LABEL maintainer "b.sarda@free.fr"


# expose
EXPOSE 8080 8442 8443

# declare vars
ENV JBOSS_HOME=/var/local/jboss \
	APPSRV_HOME=/var/local/jboss \
	EJBCA_HOME=/var/local/ejbca \
    # db vars
	DB_USER=ejbca \
	DB_PASSWORD=ejbca \
	DB_URL=jdbc:mysql://127.0.0.1:3306/ejbca?characterEncoding=UTF-8 \
	DB_DRIVER=org.mariadb.jdbc.Driver \
	DB_NAME=mysql \
    # ebjca configs
	EJBCA_CLI_USER=ejbca \
	EJBCA_CLI_PASSWORD=ejbca \
	EJBCA_KS_PASS=foo123 \
    # ca config
	CA_NAME=ManagementCA \
	CA_DN=CN=ManagementCA,O=EJBCA,C=FR \
	CA_KEYSPEC=2048 \
	CA_KEYTYPE=RSA \
	CA_SIGNALG=SHA256WithRSA \
	CA_VALIDITY=3650 \
    # web config
	WEB_SUPERADMIN=SuperAdmin \
	WEB_JAVA_TRUSTPASSWORD=changeit \
	WEB_HTTP_PASSWORD=serverpwd \
	WEB_HTTP_HOSTNAME=localhost \
	WEB_HTTP_DN=CN=localhost,O=EJBCA,C=FR \
	WEB_SELFREG=true

# add files
COPY [	"jboss-as-7.1.1.Final.tar.gz", \
	"ejbca_ce_6_3_1_1.tar.gz", \
	"mariadb-java-client-1.5.2.jar", \
	"postgresql-9.1-903.jdbc4.jar", \
	"mariadb.repo", \
	"ejbcainit.sh", \
	"jbossinit.sh", \
	"dbinit.sh", \
	"stop.sh", \
	"init.sh", \
	"/tmp/"]

# /var/local/ejbca/

# install prereq
RUN mkdir -p /var/local/ejbca/ && mkdir -p $EJBCA_HOME && mkdir -p $APPSRV_HOME && mkdir -p $JBOSS_HOME && \
	rpm --import https://yum.mariadb.org/RPM-GPG-KEY-MariaDB && \
	yum install -y net-tools java-1.7.0-openjdk java-1.7.0-openjdk-devel ant ant-optional && \
	groupadd ejbca && useradd ejbca -g ejbca && \
	mv /tmp/mariadb.repo /etc/yum.repos.d/ && \
	yum install -y MariaDB-client && \
	mv /tmp/init.sh /usr/local/bin/init.sh && mv /tmp/stop.sh /usr/local/bin/stop.sh && \
	chmod 750 /usr/local/bin/init.sh && chmod 750 /tmp/dbinit.sh && chmod 750 /tmp/jbossinit.sh && chmod 750 /tmp/ejbcainit.sh && chmod 750 /usr/local/bin/stop.sh && \
	tar xvf /tmp/jboss-as-7.1.1.Final.tar.gz -C $APPSRV_HOME && mv $APPSRV_HOME/jboss-as-7.1.1.Final/* $APPSRV_HOME/ && rm -Rf $APPSRV_HOME/jboss-as-7.1.1.Final/ && \
	tar xvf /tmp/ejbca_ce_6_3_1_1.tar.gz -C $EJBCA_HOME && mv $EJBCA_HOME/ejbca_ce_6_3_1_1/* $EJBCA_HOME/ && rm -Rf $EJBCA_HOME/ejbca_ce_6_3_1_1/ && \
	sed -i 's/jboss.bind.address.management:127.0.0.1/jboss.bind.address.management:0.0.0.0/' $APPSRV_HOME/standalone/configuration/standalone.xml

CMD ["/usr/local/bin/init.sh"]
