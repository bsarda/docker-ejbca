#!/bin/bash
touch /tmp/letitrun

# launch db init
/bin/sh -c '/tmp/dbinit.sh'

# set java home
JAVA_HOME=/usr/lib/jvm/$(ls /usr/lib/jvm/ -l | grep '^d' | awk '{print $9}')

# launch jboss init
/bin/sh -c '/tmp/jbossinit.sh'

# if already initialized or not
if [ ! -f $EJBCA_HOME/ejbcaInitialized ]; then
    echo "This is the first launch - will init the configs, jboss, ant..."
    # launch ejbca init
    /bin/sh -c '/tmp/ejbcainit.sh'
    # create flag file
    touch $EJBCA_HOME/ejbcaInitialized;

    # reload
    $APPSRV_HOME/bin/jboss-cli.sh -c --command=':reload'
    while [[ `netstat -an | grep 8080 | wc -l` == 0 ]];
    do
        echo "Wating for JBoss reload"
        sleep 1;
    done
    echo "EJBCA Initialized. do a 'docker cp' on the /superadmin.p12 file to download su token"
else
    echo "EJBCA Already initialized, no need to reinit"
fi


# wait in an infinite loop for keeping alive pid1
trap '/bin/sh -c "/usr/local/bin/stop.sh"' SIGTERM
while [ -f /tmp/letitrun ]; do sleep 1; done
exit 0;
